import Vue from 'vue';
import VueResource from 'vue-resource';
import NProgress from 'nprogress';
import auth from 'services/auth';
import _ from 'lodash';

Vue.use(VueResource);

Vue.http.interceptors.push((request, next) => {
  NProgress.start();
  next((response) => {
    NProgress.done();
    if (response.status === 500 && response.body) {
      const responseData = response.body.data;
      if (_.has(responseData, 'expiredAt')) {
        auth.logout();
      }
    }
  });
});
