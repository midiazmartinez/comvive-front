import Vue from 'vue';
import VueValidate from 'vee-validate';
import EN from 'locales/en';
import ES from 'locales/es';

Vue.use(VueValidate, {
  locale: 'en',
  dictionary: { // dictionary object
    en: EN.validator,
    es: ES.validator
  }
});
