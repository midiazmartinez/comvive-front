import Vue from 'vue';
import router from './router';
import './resource';
import './validator';
import './locale';
import 'styles/main.sass';
import auth from 'services/auth';
// Loads components
import 'components/Navigation/navigation';
import 'components/Language/language';

// Check the user's auth status when the app starts
auth.checkAuth();

export { Vue, router };
