import Vue from 'vue';
import VueI18n from 'vue-i18n';
import _ from 'lodash';
import EN from 'locales/en';
import ES from 'locales/es';

Vue.use(VueI18n);

Vue.config.lang = localStorage.getItem('lang') || 'en';

Vue.prototype.$locale = {
  change(lang) {
    Vue.config.lang = lang;
  },
  current() {
    return Vue.config.lang;
  }
};

// Set locales
_.each({
  en: EN,
  es: ES
}, (value, lang) => {
  Vue.locale(lang, value);
});
