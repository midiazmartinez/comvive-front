import Vue from 'vue';
import VueRouter from 'vue-router';
import routes from 'enums/routes';
import policies from 'services/policies';

Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  linkActiveClass: 'active',
  hashbang: false,
  root: '/',
  saveScrollPosition: true,
  routes
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(m => m.meta.auth)) {
    policies.isAuthenticated(to, from, next);
  } else if (to.matched.some(m => m.meta.guest)) {
    policies.isGuest(to, from, next);
  } else {
    next();
  }
});

export { router as default };
