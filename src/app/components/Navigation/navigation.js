import Vue from 'vue';
import template from './navigation.html';
import auth from 'services/auth';

export default Vue.component('navigation', {
  template,

  data() {
    return {
      user: auth.user
    };
  },

  methods: {
    logout() {
      auth.logout();
    }
  }
});
