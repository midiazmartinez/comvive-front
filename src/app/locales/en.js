export default {
  message: {
    jumbotronTitle: 'Manage, Monitor, Administrate',
    jumbotronSubtitle: 'ComVive the solution to stay connected and manage your community',
    titleSignup: 'Create an account ComVive',
    successSignup: 'New account created successfully',
    acceptTerms: 'I accept the',
    acceptTermsAnd: 'and',
    acceptConditions: 'You must accept the conditions.'
  },
  language: {
    en: 'English',
    es: 'Spanish'
  },
  ui: {
    language: 'Language',
    button: {
      btnStarted: 'Get started for free',
      btnSignup: 'Create a new account'
    },
    link: {
      linkTerm: 'Terms of Service',
      linkPolicy: 'Privacy Policy'
    },
    navbar: {
      login: 'Log in',
      logout: 'Log out',
      users: 'Users',
      myComvive: 'My ComVive',
      profile: 'Profile',
      changeEmail: 'Change email',
      changePassword: 'Change password'
    },
    placeholder: {
      name: 'Full Name',
      firstSurname: 'First Surname',
      secondSurname: 'Second Surname',
      username: 'Username',
      email: 'Email',
      password: 'Password',
      passwordConfirmation: 'Confirm Password'
    }
  },
  validator: {
    attributes: {
      password_confirmation: 'Confirm Password'
    }
  }
};
