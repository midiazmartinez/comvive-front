import Home from 'views/Home/home';
import NotFound from 'views/NotFound/notFound';
import Signin from 'views/Signin/signin';
import Signup from 'views/Signup/signup';
import Users from 'views/Users/users';
import Me from 'views/Me/me';
import Account from 'views/Account/account';
import Forgot from 'views/Forgot/forgot';

export default [
  { path: '/', component: Home },
  { path: '/signin', component: Signin, meta: { guest: true } },
  { path: '/signup', component: Signup, meta: { guest: true } },
  { path: '/users', component: Users, meta: { auth: true } },
  { path: '/me', component: Me, meta: { auth: true } },
  { path: '/account', component: Account, meta: { auth: true } },
  { path: '/forgot', component: Forgot, meta: { guest: true } },
  { path: '*', component: NotFound }
];
