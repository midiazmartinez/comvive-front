const API_URL = `${process.env.API_HOST}/${process.env.API_VERSION}/`;
export default {
  LOGIN: `${API_URL}auth/signin`,
  SIGNUP: `${API_URL}auth/signup`,
  REFRESH_TOKEN: `${API_URL}auth/refresh_token`,
  USERS: `${API_URL}users`,
  ME: `${API_URL}users/me`,
};
