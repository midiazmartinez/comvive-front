import router from 'boot/router';
import jwtDecode from 'jwt-decode';
import endpoint from 'endpoints';
import _ from 'lodash';

export default {
  user: {
    authenticated: false,
    name: ''
  },

  signin(context, creds, redirect) {
    context.$http.post(endpoint.LOGIN, creds).then((response) => {
      const res = response.body;
      if (res.code === 'OK' && res.data) {
        localStorage.setItem('token', res.data.token);
        this.user.authenticated = true;
        this.user.name = jwtDecode(res.data.token).name;
        if (redirect) {
          router.push(redirect);
        }
      }
    }, (response) => {
      context.onError(response.body);
    });
  },

  signup(context, creds, redirect) {
    context.$http.post(endpoint.SIGNUP, creds).then((response) => {
      const res = response.body;
      if (res.code === 'OK' && res.data) {
        localStorage.setItem('token', res.data.token);
        this.user.authenticated = true;
        this.user.name = jwtDecode(res.data.token).name;
        if (redirect) {
          router.push(redirect);
        }
      }
    }, (response) => {
      context.error = response.body.message;
    });
  },

  refresh_token(context, redirect) {
    context.$http.post(endpoint.REFRESH_TOKEN, {
      token: this.getAuthHeader()
    }).then((response) => {
      const res = response.body;
      if (res.code === 'OK' && res.data) {
        localStorage.setItem('token', res.data.token);
        this.user.authenticated = true;
        this.user.name = jwtDecode(res.data.token).name;
        if (redirect) {
          router.push(redirect);
        }
      }
    }, (response) => {
      const err = response.body;
      console.log(err.message);
      this.logout();
    });
  },

  logout() {
    localStorage.removeItem('token');
    this.user.authenticated = false;
    router.push('/');
  },

  checkAuth() {
    const token = localStorage.getItem('token');
    this.user.authenticated = !!token;
    this.user.name = token ? jwtDecode(token).name : '';
  },

  getAuthHeader() {
    const token = localStorage.getItem('token');
    return !_.isNull(token) ? {
      Authorization: `JWT ${token}`
    } : {};
  }
};
