import auth from 'services/auth';

export default {

  isAuthenticated(to, from, next) {
    if (auth.user.authenticated) {
      next();
    } else {
      next('/signin');
    }
  },

  isGuest(to, from, next) {
    if (!auth.user.authenticated) {
      next();
    } else {
      next(from.path);
    }
  }
};
