import Vue from 'vue';
import template from './account.html';

export default Vue.extend({
  template,

  data() {
    return {
      account: null,
      error: '',
    };
  },

  created() {
  },

  methods: {
  },
});
