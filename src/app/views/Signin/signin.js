import Vue from 'vue';
import _ from 'lodash';
import template from './signin.html';
import auth from 'services/auth';

export default Vue.extend({
  template,

  data() {
    return {
      credentials: {
        user: '',
        password: ''
      },
      error: ''
    };
  },

  computed: {
    checkCredetials() {
      return !(_.trim(this.credentials.user) !== '' && _.trim(this.credentials.password) !== '');
    }
  },

  methods: {
    submit() {
      auth.signin(this, {
        user: this.credentials.user,
        password: this.credentials.password
      }, 'users');
    },

    checkSubmit() {
      if (!this.checkCredetials) {
        this.submit();
      }
    },

    onError(error) {
      this.credentials.user = this.credentials.password = '';
      this.error = error.message;
    }
  }
});
