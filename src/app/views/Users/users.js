import Vue from 'vue';
import template from './users.html';
import auth from 'services/auth';
import endpoint from 'endpoints';

export default Vue.extend({
  template,

  data() {
    return {
      users: null,
      error: '',
    };
  },

  created() {
    this.getUsers();
  },

  methods: {
    getUsers() {
      this.$http.get(endpoint.USERS, {
        headers: auth.getAuthHeader()
      }).then((response) => {
        const res = response.body;
        if (res.code === 'OK' && res.data) {
          this.users = res.data;
        }
      }, (response) => {
        this.error = response.body.message;
      });
    }
  }
});
