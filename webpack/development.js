const path = require('path');
const webpack = require('webpack');
const BrowserSyncPlugin = require('browser-sync-webpack-plugin');
const merge = require('webpack-merge');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const webpackBase = require('./webpack.base.js');

module.exports = merge(webpackBase, {
  devtool: '#eval-source-map',
  plugins: [
    new BrowserSyncPlugin(
      {
        host: 'localhost',
        port: 5000,
        // Proxy the default webpack dev-server port
        proxy: 'http://localhost:8080/',
        notify: false,
        open: false,
        // Let webpack handle the reload
        codeSync: false
      }
    ),

    new webpack.DefinePlugin({
      'process.env.NODE_ENV': '"development"',
      'process.env.API_HOST': '"http://localhost:3000"',
      'process.env.API_VERSION': '"v1"'
    }),

    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),

    new HtmlWebpackPlugin({
      inject: 'body',
      template: path.resolve(__dirname, '../src/index.html')
    }),

    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin()
  ]
});
