const path = require('path');
const autoprefixer = require('autoprefixer');
const eslintFriendlyFormatter = require('eslint-friendly-formatter');

module.exports = {
  entry: {
    app: [
      path.resolve(__dirname, '../src/main.js'),
      'bootstrap-webpack!./webpack/config/bootstrap/bootstrap.config.js',
      'font-awesome-webpack!./webpack/config/font-awesome/font-awesome.config.js'
    ],
  },

  output: {
    path: path.resolve(__dirname, '../build'),
    publicPath: '/',
    filename: '[name].[hash].js',
    sourceMapFilename: '[name].[hash].js.map',
    chunkFilename: '[id].chunk.js',
  },

  resolve: {
    extensions: ['', '.js', '.html'],
    fallback: [path.join(__dirname, '../node_modules')],
    alias: {
      src: path.resolve(__dirname, '../src'),
      assets: path.resolve(__dirname, '../src/app/assets'),
      components: path.resolve(__dirname, '../src/app/components'),
      views: path.resolve(__dirname, '../src/app/views'),
      services: path.resolve(__dirname, '../src/app/services'),
      locales: path.resolve(__dirname, '../src/app/locales'),
      styles: path.resolve(__dirname, '../src/app/styles'),
      utils: path.resolve(__dirname, '../src/app/utils'),
      enums: path.resolve(__dirname, '../src/app/enums'),
      boot: path.resolve(__dirname, '../src/boot'),
      endpoints: path.resolve(__dirname, '../src/app/enums/endpoints.js'),
      vue: process.env.NODE_ENV !== 'production' ? 'vue/dist/vue.js' : 'vue/dist/vue.min.js',
    },
  },

  module: {
    preLoaders: [
      {
        test: /\.js$/,
        loader: 'eslint',
        include: path.resolve(__dirname, '../src'),
        exclude: /node_modules/,
      },
    ],

    loaders: [
      {
        test: /\.html$/,
        loader: 'vue-html',
      },

      {
        test: /\.vue$/,
        loader: 'vue',
      },

      {
        test: /\.js$/,
        loader: 'babel',
        exclude: /node_modules/,
      },

      {
        test: /\.(png|jpg|jpeg|gif)$/,
        loader: 'file-loader?name=images/[name].[ext]',
        exclude: /node_modules/
      },

      {
        test: /\.sass$/,
        loader: 'style!css!sass?sourceMap!postcss',
      },

      {
        test: /\.(ttf|eot|svg)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'file',
      },

      {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: 'url?prefix=font/&limit=5000&mimetype=application/font-woff',
      },

      { test: /bootstrap\/js\//, loader: 'imports?jQuery=jquery' }
    ],
  },

  babel: {
    presets: ['es2015'],
    plugins: ['transform-runtime'],
  },

  eslint: {
    formatter: eslintFriendlyFormatter,
  },

  postcss() {
    return [autoprefixer];
  },
};
