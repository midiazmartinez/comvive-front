# ComVive front-end

The Progressive JavaScript Framework [Vue.js](https://vuejs.org/).

Construido con:

* [Vue.js](https://github.com/vuejs/vue)
* [Vue Router](https://github.com/vuejs/vue-router)
* [Vue Resouce](https://github.com/vuejs/vue-resource)
* [Babel](https://babeljs.io/)
* [BrowserSync](https://www.browsersync.io/)
* [ESLint](http://eslint.org/)
* [Webpack](https://webpack.github.io/)
* [SASS](http://sass-lang.com/)

## Empezando

Clonar el repo & ejecutar `npm install` desde la raíz del proyecto

## Los comandos disponibles

```shell
npm start
```

Runs the Webpack module-bundler, starts watching for changes & launches the BrowserSync server to [http://localhost:5000](http://localhost:5000) (it's possible to change the port from `package.json` config-section).

**Note!** Webpack handles all the reloading stuff while BrowserSync just proxies the default webpack-port (`8080`) giving the possibility to connect to dev-server from multiple devices.

```shell
npm run lint:js
```

Lints javascript-files inside `/src` directory

```shell
npm run build
```
Ejecuta el webpack module-bundler con los ajustes de producción (comprimir etc.) y construye el proyecto en el directorio `/build`.